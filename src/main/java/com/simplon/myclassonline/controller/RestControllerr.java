package com.simplon.myclassonline.controller;

import java.util.List;

import com.simplon.myclassonline.dao.ClassRoomRepository;
import com.simplon.myclassonline.dao.UserRepository;
import com.simplon.myclassonline.model.ClassRoom;
import com.simplon.myclassonline.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestControllerr {
    @Autowired
   public UserRepository repo;
   @Autowired
   public ClassRoomRepository repoclass;
    
   //to show all teachers with javascript
   @GetMapping("/api/displayAllTeachers")
    public List<User> showAllTeachers(){
        List<User> teachers = repo.findAll();
        return teachers;
    }

     //to show all teachers with javascript in the admin page
   @GetMapping("/api/displayAllTeas")
   public List<User> showTeachers(){
       List<User> teas = repo.findAll();
       return teas;
   }

   //to show allclassrooms in the admin page
   @GetMapping("/api/displayAllClassrooms")
   public List<ClassRoom> showAllClassrooms(){
       List<ClassRoom> classRooms = repoclass.findAll();
       return classRooms;
   }



  
}
