package com.simplon.myclassonline.controller;

import javax.validation.Valid;

import com.simplon.myclassonline.dao.UserRepository;
import com.simplon.myclassonline.model.User;
import com.simplon.myclassonline.servise.Uploader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class UserController {
    @Autowired
    private Uploader uploader;
    @Autowired
    public UserRepository repo;
    @Autowired
    private PasswordEncoder encoder;

    // showing the registration page
    @GetMapping("/Registration")
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "Registration";
    }

    // adding a user in the registration form and it will be like adding a teacher
    @PostMapping("/Registration")
    public String addUser(@Valid User user, BindingResult result, Model model,
            @RequestParam("file") MultipartFile file) {
        if (result.hasErrors()) {
            return "Registration";
        }

        if (repo.findByEmail(user.getEmail()) != null) {
            model.addAttribute("feedback", "User already exists");
            return "Registration";
        }
        try {
            String hashedPassword = encoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
            user.setRole("ROLE_ADMIN");
            user.setImage(uploader.upload(file));
            repo.add(user);

        } catch (Exception e) {
            e.printStackTrace();

        }

        return "redirect:/";
    }

    // to show all of teachers with thymeleaf
    // @GetMapping("/showAllTeachers")
    // public String showAllTeachers(Model model){
    // model.addAttribute("teachers",repo.findAll());
    // return "showAllTeachers";
    // }

    @GetMapping("/showAllTeachers")
    public String showAllTeachers() {
        return "showAllTeachers";
    }

    // this method is for showing the profil with thymeleaf
    @GetMapping("/profile/{id}")
    public String findById(@PathVariable int id , Model model) {
    User user = repo.findById(id);
    model.addAttribute("teacher", user);
    return "profile";
    }


//for update user 

// @GetMapping("/updateUser")
// public String updateUser(){
//     return "/updateUser";
// }

// @PostMapping("/updateUser/{email}")
// public String update(@PathVariable("email") String email) {
//     User user = repo.findByEmail(email);
//     user.setRole("ROLE_USER");
//     repo.updateById(user);
//     return "redirect:/updateUser";
// }

@GetMapping("/updateUser")
public String showEdit(Model model, @AuthenticationPrincipal User user) {
    model.addAttribute("newUser", user);
    return "updateUser";
}

@PostMapping("/updateUser")
public String modifyProfil(User newUser, @AuthenticationPrincipal User user) {

    user.setFirstName(newUser.getFirstName());
    user.setLastName(newUser.getLastName());
    user.setDescription(newUser.getDescription());
    user.setEmail(newUser.getEmail());

    // update user in database
    repo.updateById(user);

    return "redirect:/";
}

    // just to show the profile page whiche il use it with js from the rest CONTROLLER
    // @GetMapping("/profile/{id}")
    // public String findById() {
    //     return "profile";
    // }

    // to check it later it does'nt work yet
    // @GetMapping("/teachers/{id}")
    // public String showTeachersList(Model model,@PathVariable Integer id){
    // model.addAttribute("teacher", repo.findAll(id));
    // return "showTeacherList";
    // }


}
