package com.simplon.myclassonline.controller;




import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class ShowPagesHtml {


    @GetMapping ("/user")
    public String admin(){
        return "user";
}
    @GetMapping ("/about")
    public String about(){
        return "about";
}

@GetMapping ("/contact")
public String contact(){
    return "contact.html";
}
@GetMapping ("/teacher")
public String teacher(){
    return "teacher.html";
}
@GetMapping ("/myClasses")
public String lesson(){
    return "myClasses.html";
}
@GetMapping ("/admin")
public String test(){
    return "admin.html";
}
// @GetMapping ("/signin")
// public String signin(){
//     return "signin.html";
// }

// @GetMapping("/default")
// public String defaultAfterLogin(HttpServletRequest request) {

//     if (request.isUserInRole("ROLE_ADMIN")) {
//         return "/";
//     }
//     return "redirect:/";
// }
}
